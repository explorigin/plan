#!/bin/bash

PROJECT_ROOT=/srv

function install_packages {
    apt-get update
    apt-get upgrade
    cat $PROJECT_ROOT/configuration/dpkg-requirements | xargs sudo apt-get install -y -q
}

function initialize_postgres {
    # FIXME - www-data user should not be a superuser in postgres
    yes | sudo -u postgres createuser -D -A -P www-data
    sudo -u postgres createdb -O www-data plan
}

function install_python_requirements {
    pip install -r $PROJECT_ROOT/requirements.txt
}

function install_nginx {
    cp $PROJECT_ROOT/configuration/nginx/plan.conf /etc/nginx/sites-available/
    ln -sf /etc/nginx/sites-available/plan.conf /etc/nginx/sites-enabled/plan
    rm -f /etc/nginx/sites-enabled/default
}

function install_supervisor {
    cp $PROJECT_ROOT/configuration/supervisor/plan.conf /etc/supervisor/conf.d/
}

function initialize_plan {
    mkdir /run/plan
    $PROJECT_ROOT/manage.py init
    chown -R vagrant:vagrant /run/plan
}

function start_services {
    service nginx start
    supervisorctl reload
}

install_packages
install_python_requirements
install_nginx
install_supervisor

initialize_postgres
initialize_plan

start_services
