Plan
====

Plan is a headstart on your next large project. It builds on Flask's tutorial
to incorporate featureful components that would be used in a large project.

Plan is built on Flask and many of its better extensions.  It also is structured
in a way that seperates extension initialization and configuration code from
application code.

Then end goal of Plan is to have a system that allows you to plug in your models
and immediately have many features available to you so you spend more time
working on your app than you do integrating features.
