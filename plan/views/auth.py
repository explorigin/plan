from flask import request, redirect, url_for, render_template, flash

from flask.ext.principal import AnonymousIdentity, Identity, identity_changed
from flask.ext.login import login_user, logout_user

from ..core.authentication import User

from .. import app

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            user = User()
            login_user(user)

            # Tell Flask-Principal the identity changed
            identity_changed.send(app, identity=Identity(user.id))

            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    logout_user()

    # Tell Flask-Principal the user is anonymous
    identity_changed.send(app, identity=AnonymousIdentity())
    flash('You were logged out')
    return redirect(url_for('show_entries'))