from flask import request, redirect, url_for, abort, render_template, flash

from flask.ext.login import login_required, current_user

from .. import app
from ..core import db
from ..permissions import add_entry_permission
from ..models.entry import Entry

@app.route('/')
def show_entries():
    return render_template('show_entries.html',
                           entries=Entry.query.all(),
                           current_user=current_user
    )

@app.route('/add', methods=['POST'])
@add_entry_permission.require()
def add_entry():
    db.session.add(Entry(title=request.form['title'],
                         text=request.form['text']))
    db.session.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))
