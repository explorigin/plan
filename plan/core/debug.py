try:
    from flask.ext.debugtoolbar import DebugToolbarExtension
except ImportError:
    pass
else:
    from .. import app
    toolbar = DebugToolbarExtension(app)
