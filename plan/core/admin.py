import os.path as op

from flask.ext.admin import Admin
from flask.ext.admin.contrib.fileadmin import FileAdmin

from .. import app

admin = Admin(app)

# File-Admin
admin.add_view(FileAdmin(
    app.config['STATIC_PATH'],
    app.config['STATIC_URL'],
    name='Static Files'))