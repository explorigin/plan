from flaskext_compat import activate
activate()

from .admin import admin
from .api import manager as api_manager
from .assets import assets
from .authentication import login_manager
from .authorization import principals
from .cache import *
from .debug import *
from .orm import db
