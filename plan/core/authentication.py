from flask import request, redirect, flash, render_template, url_for
from flask.ext.login import LoginManager, current_user, login_required

from .. import app

login_manager = LoginManager()
login_manager.init_app(app)

class User:
    roles = ('editor',)

    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return u"1"

    @property
    def id(self):
        return int(self.get_id())


@login_manager.user_loader
def load_user(userid):
    return User()
