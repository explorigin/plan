from flask.ext.script import Manager, Server

from .. import app

manager = Manager(app)

manager.add_command("runserver", Server(host="0.0.0.0", port=5000))

@manager.command
def tornado():
    "Serve Plan app via Tornado."

    from tornado.options import options, define, parse_command_line
    import tornado.httpserver
    import tornado.ioloop
    import tornado.web
    import tornado.wsgi

    define('port', type=int, default=9987)

    # If you want to add tornado handlers, this is a good place to start.
    #class HelloHandler(tornado.web.RequestHandler):
    #  def get(self):
    #    self.write('Hello from tornado')

    wsgi_app = tornado.wsgi.WSGIContainer(app)
    tornado_app = tornado.web.Application(
        [
    #        ('/hello-tornado', HelloHandler),
            ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
        ])
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

@manager.command
def init():
    """Initialize a new instance of this app."""

    # Create the tables
    from .orm import db
    db.create_all()
