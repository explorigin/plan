import os

from flask.ext.cache import Cache

from .. import app

caches = app.config.get('CACHES', ())

for name, config in caches.items():
    locals()[name] = Cache(app, config=config)

if len(caches):
    try:
        os.makedirs('/tmp/plan-cache')
    except:
        pass
