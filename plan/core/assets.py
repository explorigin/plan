from flask.ext.assets import Environment

from .. import app

assets = Environment(app)

try:
    from flask.ext.assets import ManageAssets
except ImportError:
    # Warning
    print "Error importing ManageAssets."
else:
    from .commands import manager
    manager.add_command("assets", ManageAssets(assets))
