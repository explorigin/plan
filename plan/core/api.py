from flask.ext.restless import APIManager

from .. import app
from .orm import db

# Construct our API manager
manager = APIManager(app, flask_sqlalchemy_db=db)
