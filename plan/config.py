from os import path as op

# configuration

MODE = 'dev' # 'staging' or production
DEBUG = True
TESTING = False

BASE_PATH = '/srv'
STATIC_URL = '/static'
STATIC_PATH = op.join(BASE_PATH, 'plan/static')
SECRET_KEY = 'Drink more ovaltine'

# ORM
SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://www-data:y@localhost/plan'
USERNAME = 'admin'
PASSWORD = 'default'

# Web Assets
ASSETS_URL = STATIC_URL
ASSETS_DIRECTORY = STATIC_PATH
ASSETS_CACHE = False if DEBUG else '/tmp'
ASSETS_MANIFEST = None

# Cache(s)
CACHES = {
    'ram_cache': {
        'CACHE_TYPE': 'simple',
        'CACHE_DEFAULT_TIMEOUT': 60, # seconds
    },
    'disk_cache': {
        'CACHE_TYPE': 'filesystem',
        'CACHE_DEFAULT_TIMEOUT': 3600, # seconds, 1 hour
        'CACHE_DIR': '/tmp/plan-cache'
    }
}
