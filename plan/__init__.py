# all the imports
from flask import Flask

# create our little application :)
app = Flask(__name__)
app.config.from_object('plan.config')

# Add Views
from . import views, bundles

if __name__ == '__main__':
    app.run()
