from flask.ext.principal import RoleNeed, Permission

# Build your needs and permissions here
add_entry_permission = Permission(RoleNeed("editor"))