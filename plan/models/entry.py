from flask.ext.admin.contrib.sqlamodel import ModelView

from ..core import api_manager, db

class Entry(db.Model):
    __tablename__ = 'entries'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True)
    text = db.Column(db.String(1024), unique=False)

    def __init__(self, *args, **kwargs):
        self.__dict__.update(**kwargs)

    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__, self.title)

# Expose via REST
api_manager.create_api(Entry, methods=['GET', 'POST', 'DELETE'])

# Expose in Admin interface
from ..core import admin
admin.add_view(ModelView(Entry, db.session))
