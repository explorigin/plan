from flask.ext.assets import Bundle

from .core import assets

#### Bundles

# CSS
assets.register(
    'css_all',
    Bundle(
        'scss/style.scss',
        filters='pyscss',
        output='_gen/style.css'
    )
)
